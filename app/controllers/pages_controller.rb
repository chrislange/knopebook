class PagesController < ApplicationController
	def home
	
		@user = current_user
		if user_signed_in?
			@post = @user.posts.build
			@project = @user.projects.build
			
			@announcements = @user.posts.where({post_type: 1})
			@compliments = @user.posts.where({post_type: 2})
			@projects = @user.projects

		end
	end

end