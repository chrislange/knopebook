class UsersController < ApplicationController

	def index
		@users = User.all
	end

	def show
		@user = current_user
		@post = @user.posts.new
		@posts = @user.posts	
		@project = @user.projects.new
	end
end